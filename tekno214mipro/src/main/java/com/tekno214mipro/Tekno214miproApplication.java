package com.tekno214mipro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tekno214miproApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tekno214miproApplication.class, args);
	}

}
