package com.tekno214mipro.repository;

import com.tekno214mipro.model.usersModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface loginRepository extends JpaRepository<usersModel,Integer> {
    @Query("select u.nama from usersModel u where u.nama=?1 and u.sandi=?2")
    String ceklogin(String user, String pass);

}
