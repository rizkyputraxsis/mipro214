package com.tekno214mipro.repository;

import com.tekno214mipro.model.usersModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface usersRepository extends JpaRepository<usersModel,Integer> {


@Query("select u from usersModel u where u.is_delete !=true order by u.iduser")
List<usersModel> semuauser();

//select by id
@Query("select u from usersModel u where u.iduser= ?1")
usersModel userbyid(int ids);

//delete flag
@Modifying
@Query("UPDATE usersModel SET is_delete=true WHERE id_user=?1")
int deletflag(int ids);

}
