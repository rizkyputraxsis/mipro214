package com.tekno214mipro.repository;

import com.tekno214mipro.model.supplierModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface supplierRepository extends JpaRepository<supplierModel,Integer> {


@Query("select s from supplierModel s where s.nama like %?1% or s.email like %?1% order by s.idsupplier")
List<supplierModel> semuasupplier(String seperti);

//select by id
@Query("select s from supplierModel s where s.idsupplier= ?1")
supplierModel supplierbyid(int ids);

//delete flag
@Modifying
@Query("UPDATE supplierModel SET is_delete=true WHERE id_user=?1")
int deletflag(int ids);

//cekmail
    @Query("select s.email from supplierModel s where s.email = ?1")
    String cekmail(String mailn);
}
