package com.tekno214mipro.repository;

import com.tekno214mipro.model.provinsiModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface provinsiRepository extends JpaRepository<provinsiModel,String> {

}
