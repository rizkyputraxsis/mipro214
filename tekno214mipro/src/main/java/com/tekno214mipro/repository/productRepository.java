package com.tekno214mipro.repository;

import com.tekno214mipro.dtomodel.vwproduct;
import com.tekno214mipro.model.productModel;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface productRepository extends JpaRepository<productModel,Integer> {


@Query("select p from productModel p order by p.idproduct")
List<productModel> semuaproduct();

    @Query(
 "select new com.tekno214mipro.dtomodel.vwproduct(p.idproduct,p.nama as namaproduct,p.brand," +
 "p.madein,p.price,s.nama as namasupplier) from productModel p left join supplierModel s on p.idsupplier = s.idsupplier")
    List<vwproduct> semuaproductj();

//select by id
@Query("select p from productModel p where p.idproduct= ?1")
productModel productbyid(int ids);

//delete flag
/*@Modifying
@Query("UPDATE usersModel SET is_delete=true WHERE id_user=?1")
int deletflag(int ids);*/

}
