package com.tekno214mipro.controllrt;

import com.tekno214mipro.dtomodel.vwproduct;
import com.tekno214mipro.model.productModel;
import com.tekno214mipro.model.supplierModel;
import com.tekno214mipro.service.productService;

import com.tekno214mipro.service.supplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class productController {
    @Autowired
    private productService prs;
    @Autowired
    private supplierService sss;

    @RequestMapping("/product")
    public String product(){
        return "product/product";
    }

    @RequestMapping("/productlist")
    public String productlist(Model model){
        List<vwproduct> productlist = prs.semuaproductj();
        model.addAttribute("productlist",productlist);
        return "product/productlist";
    }

    @RequestMapping("/productadd")
    public String productadd(Model model){
        List<supplierModel>listsplr=sss.semuasupplier("");
        model.addAttribute("listsplr",listsplr);
        return "product/productadd";
    }

    @ResponseBody
    @RequestMapping("/productsave")
    public String productsave(@ModelAttribute("datanewproduct")productModel prm){


        prs.save(prm);
        return "#";
    }

    @RequestMapping("/productformedit/{ids}")
    public String productformedit(
            @PathVariable(name = "ids") int ids, Model model){
        productModel productbyid = prs.productbyid(ids);
        model.addAttribute("productbyid",productbyid);
        List<supplierModel>listsplr=sss.semuasupplier("");
        model.addAttribute("listsplr",listsplr);

        return "product/productedit";
    }

   /* @RequestMapping("/usersformdelete/{ids}")
    public String usersformdelete(
            @PathVariable(name = "ids") int ids, Model model){
        usersModel userbyid = uss.userbyid(ids);
        model.addAttribute("userbyid",userbyid);
        return "users/usersdelete";
    }*/

   /* @ResponseBody
    @RequestMapping("/userdelete/{ids}")
    public String userdelete(@PathVariable(name = "ids")int ids) {
        uss.deleteById(ids);
        return "#";
    }*/



}
