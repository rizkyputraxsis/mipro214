package com.tekno214mipro.controllrt;

import com.tekno214mipro.dtomodel.newinsuppro;
import com.tekno214mipro.dtomodel.vwproduct;
import com.tekno214mipro.model.productModel;
import com.tekno214mipro.model.supplierModel;
import com.tekno214mipro.service.productService;
import com.tekno214mipro.service.supplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class insupproController {
    @Autowired
    private productService prs;
    @Autowired
    private supplierService sss;

    @RequestMapping("/insuppro")
    public String product(){
        return "insuppro/insuppro";
    }

    @RequestMapping("/insupprolist")
    public String productlist(Model model){
        List<vwproduct> insupprolist = prs.semuaproductj();
        model.addAttribute("insupprolist",insupprolist);
        return "insuppro/insupprolist";
    }

    @ResponseBody
    @RequestMapping("/insupprosave")
    public String insupprosave(@ModelAttribute("datanewinsuppro")newinsuppro isp){
        supplierModel spm = new supplierModel();
        productModel prm = new productModel();

        //supplier
        spm.setNama(isp.getNamasupplier());
        spm.setAlamat(isp.getAlamat());
        spm.setEmail(isp.getEmail());
        sss.save(spm);

        //product
        prm.setNama(isp.getNamaproduct());
        prm.setBrand(isp.getBrand());
        prm.setMadein(isp.getMadein());
        prm.setPrice(isp.getPrice());
        prm.setIdsupplier(spm.getIdsupplier());

        prs.save(prm);
        return "#";
    }


}
