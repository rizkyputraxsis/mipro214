package com.tekno214mipro.controllrt;

import com.tekno214mipro.model.provinsiModel;
import com.tekno214mipro.model.supplierModel;
import com.tekno214mipro.model.usersModel;
import com.tekno214mipro.service.supplierService;
import com.tekno214mipro.service.usersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class supplierController {
    @Autowired
    private supplierService sss;


    @RequestMapping("/supplier")
    public String supplier(){
        return "supplier/supplier";
    }

    @RequestMapping("/supplierlist/{seperti}")
    public String supplierlist(@PathVariable(name = "seperti") String seperti,Model model){
        if(seperti.equals("all")){
            seperti="";
        }
        List<supplierModel> supplierlist = sss.semuasupplier(seperti);
        model.addAttribute("supplierlist",supplierlist);
        return "supplier/supplierlist";
    }

    @RequestMapping("/supplieradd")
    public String supplieradd(){
        return "supplier/supplieradd";
    }

    @ResponseBody
    @RequestMapping("/suppliersave")
    public String suppliersave(@ModelAttribute("datanewsupplieru")supplierModel spm){
        String hasil="nilai";

        String cekmail = sss.cekmail(spm.getEmail());
        if(cekmail==null){
             sss.save(spm);
            hasil="1";
        }else{
            hasil ="2";
        }
        return hasil;
    }

    @ResponseBody
    @RequestMapping("/tesbody")
    public String tesbody(){
        String hasil="LALALAL yipikaie";

        return hasil;
    }

    @RequestMapping("/supplierformedit/{ids}")
    public String supplierformedit(
            @PathVariable(name = "ids") int ids, Model model){
        supplierModel supplierbyid = sss.supplierbyid(ids);
        model.addAttribute("supplierbyid",supplierbyid);
        return "supplier/supplieredit";
    }

    @RequestMapping("/supplierformdelete/{ids}")
    public String supplierformdelete(
            @PathVariable(name = "ids") int ids, Model model){
        supplierModel supplierbyid = sss.supplierbyid(ids);
        model.addAttribute("supplierbyid",supplierbyid);
        return "supplier/supplierdelete";
    }

    @ResponseBody
    @RequestMapping("/supplierdelete/{ids}")
    public String supplierdelete(@PathVariable(name = "ids")int ids) {
        sss.deleteById(ids);
        return "#";
    }

    @ResponseBody
    @RequestMapping("/semuaprovinsi")
    public List<provinsiModel> semuaprovinsi() {
        List<provinsiModel> semuaprovinsi = sss.semuaprovinsi();
        return semuaprovinsi;
    }



}
