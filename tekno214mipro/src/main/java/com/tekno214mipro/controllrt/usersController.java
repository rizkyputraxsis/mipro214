package com.tekno214mipro.controllrt;

import com.tekno214mipro.model.usersModel;
import com.tekno214mipro.service.usersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class usersController {
    @Autowired
    private usersService uss;

    @RequestMapping("/users")
    public String users(){
        return "users/users";
    }

    @RequestMapping("/userslist")
    public String userslist(Model model){
        //by jpa
       // List<usersModel> userlist = uss.listall();
        //by custom query
        List<usersModel> userlist = uss.semuauser();
        model.addAttribute("userlist",userlist);
        return "users/userslist";
    }

    @RequestMapping("/usersadd")
    public String usersadd(){
        return "users/usersadd";
    }

    @ResponseBody
    @RequestMapping("/usersave")
    public String usersave(@ModelAttribute("datanewusers")usersModel usm){
        String diedit="214";
        usm.setEditby(diedit);
        uss.save(usm);
        return "#";
    }

    @RequestMapping("/usersformedit/{ids}")
    public String usersformedit(
            @PathVariable(name = "ids") int ids, Model model){
        usersModel userbyid = uss.userbyid(ids);
        model.addAttribute("userbyid",userbyid);
        return "users/usersedit";
    }

    @RequestMapping("/usersformdelete/{ids}")
    public String usersformdelete(
            @PathVariable(name = "ids") int ids, Model model){
        usersModel userbyid = uss.userbyid(ids);
        model.addAttribute("userbyid",userbyid);
        return "users/usersdelete";
    }

    @ResponseBody
    @RequestMapping("/userdelete/{ids}")
    public String userdelete(@PathVariable(name = "ids")int ids) {
    uss.deleteById(ids);
    return "#";
    }

}
