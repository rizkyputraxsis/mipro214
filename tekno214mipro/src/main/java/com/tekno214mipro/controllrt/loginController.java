package com.tekno214mipro.controllrt;

import com.tekno214mipro.service.loginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class loginController {
    @Autowired
    private loginService ls;

    @RequestMapping("/login")
    public String login(HttpSession session){
        Object isiloguser = session.getAttribute("loguser");
        if(isiloguser == null){
            isiloguser = new String();
        }
        String logses = isiloguser.toString();
        String url="";
        if(logses.equals("")){
            url="/login";
        }else{
            url="redirect:/";
        }
        return url;
    }


    @RequestMapping(value = "/signin", method = RequestMethod.POST)
    public String signin(@RequestParam(value = "user",required = false) String user,
                         @RequestParam(value = "pass",required = false) String pass,
                         HttpServletRequest request){
        String ceklogin = ls.ceklogin(user,pass);
        String url="";
        if(ceklogin != null){
            request.getSession().setAttribute("loguser",user);
            url="redirect:/";
        }else{
            url="redirect:/login";
        }

        return url;
    }

    @RequestMapping("/logout")
    public String logout(HttpServletRequest request){
        request.getSession().invalidate();
        return "login";
    }
}
