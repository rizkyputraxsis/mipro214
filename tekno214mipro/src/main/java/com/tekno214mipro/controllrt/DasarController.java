package com.tekno214mipro.controllrt;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class DasarController {

    @RequestMapping("/")
    public String awal(HttpSession session){

        Object isiloguser = session.getAttribute("loguser");
        if(isiloguser == null){
            isiloguser = new String();
        }
        String logses = isiloguser.toString();

        String url="";

        if(!logses.equals("")){
            url="/awal";
        }else{
            url="redirect:/login";
        }
        return url;

    }



}
