package com.tekno214mipro.controllrt;

import com.tekno214mipro.dtomodel.vwproduct;
import com.tekno214mipro.model.provinsiModel;
import com.tekno214mipro.model.supplierModel;
import com.tekno214mipro.service.productService;
import com.tekno214mipro.service.supplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/panas/")
public class apitTest {
    @Autowired
    private productService prs;

    @Autowired
    private supplierService sss;

    @GetMapping("liatp")
    public ResponseEntity<List<vwproduct>> liatp() {
        return ResponseEntity.ok(prs.semuaproductj());
    }

    @GetMapping("liatp2")
    public List<supplierModel> liatp2() {
        List<supplierModel> dt = sss.semuasupplier("");

        return dt;
    }


    @GetMapping("liatv")
    public ResponseEntity<List<provinsiModel>> liatv() {
        return ResponseEntity.ok(sss.semuaprovinsi());
    }


}
