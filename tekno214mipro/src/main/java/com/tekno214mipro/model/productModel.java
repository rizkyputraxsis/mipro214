package com.tekno214mipro.model;

import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "product")
public class productModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_product")
    private int idproduct;

    @Column(name="nama")
    private String nama;

    @Column(name="brand")
    private String brand;

    @Column(name="madein")
    private String madein;

    //@NumberFormat(style = Style.CURRENCY)
    @NumberFormat(style = NumberFormat.Style.DEFAULT)
    @Column(name="price")
    private double price;

    @Column(name="id_supplier")
    private Integer idsupplier;

    //join metod 1
  /*  @ManyToOne
    @JoinColumn(name="id_supplier",nullable = true,updatable = false, insertable = false)
    private supplierModel splr;*/

    public int getIdproduct() {
        return idproduct;
    }

    public void setIdproduct(int idproduct) {
        this.idproduct = idproduct;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getMadein() {
        return madein;
    }

    public void setMadein(String madein) {
        this.madein = madein;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getIdsupplier() {
        return idsupplier;
    }

    public void setIdsupplier(Integer idsupplier) {
        this.idsupplier = idsupplier;
    }
}
