package com.tekno214mipro.model;

import javax.persistence.*;

@Entity
@Table(name = "provinsi")
public class provinsiModel {
    @Id
    @Column(name="id_provinsi")
    private String idprovinsi;

    @Column(name="nama")
    private String nama;

    public String getIdprovinsi() {
        return idprovinsi;
    }

    public void setIdprovinsi(String idprovinsi) {
        this.idprovinsi = idprovinsi;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
