package com.tekno214mipro.service;

import com.tekno214mipro.model.provinsiModel;
import com.tekno214mipro.model.supplierModel;
import com.tekno214mipro.repository.provinsiRepository;
import com.tekno214mipro.repository.supplierRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class supplierService {
    @Autowired
    private supplierRepository ssr;

    @Autowired
    private provinsiRepository pvr;

    public List<provinsiModel> semuaprovinsi(){
        return pvr.findAll();
    }


    public List<supplierModel> semuasupplier(String seprti){

        return ssr.semuasupplier(seprti);
    }


    //untuk simpan
    public supplierModel save(supplierModel suppliermodel){
        return ssr.save(suppliermodel);
    }

    public supplierModel supplierbyid(int ids){
        return ssr.supplierbyid(ids);
    }
    public void deleteById (int ids){
        //delte default jpa
        ssr.deleteById(ids);

        //delet by custom
       // ssr.deletflag(ids);
    }

    public String cekmail(String mailn)   {
          return   ssr.cekmail(mailn);
    }

}
