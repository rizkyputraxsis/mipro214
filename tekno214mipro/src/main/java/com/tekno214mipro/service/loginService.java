package com.tekno214mipro.service;

import com.tekno214mipro.repository.loginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class loginService {
    @Autowired
    private loginRepository lr;

    public String ceklogin(String user, String pass){
        return lr.ceklogin(user,pass);
    }
}
