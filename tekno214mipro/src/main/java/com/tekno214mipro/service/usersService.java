package com.tekno214mipro.service;

import com.tekno214mipro.model.usersModel;
import com.tekno214mipro.repository.usersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class usersService {
    @Autowired
    private usersRepository usr;

    public List<usersModel> listall(){
        return usr.findAll();
    }

    public List<usersModel> semuauser(){
        return usr.semuauser();
    }


    //untuk simpan
    public usersModel save(usersModel usermodel){
        return usr.save(usermodel);
    }

    public usersModel userbyid(int ids){
        return usr.userbyid(ids);
    }

    public void deleteById (int ids){
        //delte default jpa
        //usr.deleteById(ids);

        //delet by custom
        usr.deletflag(ids);
    }

}
