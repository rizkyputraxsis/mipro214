package com.tekno214mipro.service;

import com.tekno214mipro.dtomodel.vwproduct;
import com.tekno214mipro.model.productModel;
import com.tekno214mipro.repository.productRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class productService {
    @Autowired
    private productRepository prr;



    public List<productModel> semuaproduct(){
        return prr.semuaproduct();
    }

    public List<vwproduct> semuaproductj(){
        return prr.semuaproductj();
    }

    //untuk simpan
    public productModel save(productModel productmodel){
        return prr.save(productmodel);
    }

    public productModel productbyid(int ids){
        return prr.productbyid(ids);
    }
   /* public void deleteById (int ids){
        //delte default jpa
        //usr.deleteById(ids);

        //delet by custom
        usr.deletflag(ids);
    }*/

}
