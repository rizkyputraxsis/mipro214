package com.tekno214mipro.dtomodel;

public class newinsuppro {
    //modal supplier
    private String namasupplier;
    private String alamat;
    private String email;
    //modal product
    private String namaproduct;
    private String brand;
    private String madein;
    private double price;

    public newinsuppro(String namasupplier, String alamat, String email, String namaproduct, String brand, String madein, double price) {
        this.namasupplier = namasupplier;
        this.alamat = alamat;
        this.email = email;
        this.namaproduct = namaproduct;
        this.brand = brand;
        this.madein = madein;
        this.price = price;
    }

    public String getNamasupplier() {
        return namasupplier;
    }

    public void setNamasupplier(String namasupplier) {
        this.namasupplier = namasupplier;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNamaproduct() {
        return namaproduct;
    }

    public void setNamaproduct(String namaproduct) {
        this.namaproduct = namaproduct;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getMadein() {
        return madein;
    }

    public void setMadein(String madein) {
        this.madein = madein;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
