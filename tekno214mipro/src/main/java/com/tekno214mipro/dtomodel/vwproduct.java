package com.tekno214mipro.dtomodel;

public class vwproduct {
    private int idproduct;
    private String namaproduct;
    private String brand;
    private String madein;
    private double price;
    private String namasupplier;

    public vwproduct(int idproduct, String namaproduct, String brand, String madein, double price, String namasupplier) {
        this.idproduct = idproduct;
        this.namaproduct = namaproduct;
        this.brand = brand;
        this.madein = madein;
        this.price = price;
        this.namasupplier = namasupplier;
    }

    public int getIdproduct() {
        return idproduct;
    }

    public void setIdproduct(int idproduct) {
        this.idproduct = idproduct;
    }

    public String getNamaproduct() {
        return namaproduct;
    }

    public void setNamaproduct(String namaproduct) {
        this.namaproduct = namaproduct;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getMadein() {
        return madein;
    }

    public void setMadein(String madein) {
        this.madein = madein;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getNamasupplier() {
        return namasupplier;
    }

    public void setNamasupplier(String namasupplier) {
        this.namasupplier = namasupplier;
    }
}
